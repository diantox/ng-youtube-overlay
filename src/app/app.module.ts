import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { YoutubeOverlayModule } from 'ng-youtube-overlay';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    YoutubeOverlayModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
