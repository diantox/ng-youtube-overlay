import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {

  readonly videoIdRegExp: RegExp = /^[0-9A-Za-z_-]{10}[048AEIMQUYcgkosw]$/;
  readonly errorMessage: string = ' is not a valid YouTube video ID.';

  constructor(private sanitizer: DomSanitizer) {}

  transform(value: any, args?: any): any {
    if (this.videoIdRegExp.test(value)) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(value);
    } else {
      throw new RangeError(value + this.errorMessage);
    }
  }
}
