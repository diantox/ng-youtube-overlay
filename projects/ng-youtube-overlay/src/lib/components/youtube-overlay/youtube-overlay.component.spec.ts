import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubeOverlayComponent } from './youtube-overlay.component';

describe('YoutubeOverlayComponent', () => {
  let component: YoutubeOverlayComponent;
  let fixture: ComponentFixture<YoutubeOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoutubeOverlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
