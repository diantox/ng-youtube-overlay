/*
 * Public API Surface of ng-youtube-overlay
 */

export * from './lib/components/youtube-overlay/youtube-overlay.component';
export * from './lib/ng-youtube-overlay.module';
